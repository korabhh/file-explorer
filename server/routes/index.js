var express = require('express');
var readdirp = require('readdirp');
var router = express.Router();
var fs = require('fs-extra');
var pathUtil = require('path');
var formidable = require('formidable');
var http = require('http');
var util = require('util');

let root = './public/explorer';

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/files/move', function(req, res, next) {

	var moveTo = req.query.to;

	var movedFiles = [];

	for(var i in req.query.files) {
		var file = JSON.parse( req.query.files[i] );

		fs.moveSync( pathUtil.join( root , file.path ), pathUtil.join( root ,moveTo, file.fileName ) );

		movedFiles.push( getItemObject( pathUtil.join( moveTo, file.fileName ) ) );
	}

	res.json({
		items: movedFiles
	});
});


router.post('/files/create', function(req, res, next) {

	var dirName = req.body.params.dirName;
	var path = req.body.params.path;

	var dir = pathUtil.join( root , path , dirName );

	if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);

    var stat = fs.statSync( dir );

		res.json({
			success: true,
			dir: getItemObject( pathUtil.join( path , dirName ) )
		});
	} else {
		res.json({
			success: false,
			message: 'Directory exists'
		});
	}
});


router.post('/files/upload', function(req, res, next) {

	var form = new formidable.IncomingForm();

	form.uploadDir = root;

  form.parse(req, function(err, fields, files) {
  	var filePath = pathUtil.join( root, fields.path, files.file.name );

    fs.renameSync(files.file.path, filePath );

    res.json({file: getItemObject( pathUtil.join( fields.path, files.file.name ) ) });
  });

});


router.post('/files/delete', function(req, res, next) {

	var path = req.body.params.path;

	try {
		var pathToDelete = pathUtil.join( root, path );

		if (fs.existsSync( pathToDelete )){
			var stat = fs.statSync(pathToDelete);

			if( !stat.isDirectory() ) {
				fs.unlinkSync( pathToDelete );
			} else {
				fs.removeSync( pathToDelete );
			}

			res.json({
				success: true
			});

		} else {
			res.json({
				success: false,
				message: 'Does not exist'
			});
		}
	}catch(e) {
		res.json({
			success: false,
			message: e.message
		});
	}

});



router.get('/files/rename', function(req, res, next) {

	var file = req.query.file;
	var newFilename = req.query.newFilename;

	var parsed = pathUtil.parse( file );

	var target = pathUtil.join( root , parsed.dir, newFilename ); 
	var current = pathUtil.join( root , file );

	var renamedFile = null;

	try { 
		if( fs.existsSync( current ) ) {
			fs.renameSync( current , target );
			renamedFile = getItemObject( pathUtil.join( parsed.dir, newFilename ) );
		} else {
			res.json({});
		}
	} catch(e) { console.log(e) };
	
	res.json({
		item: renamedFile
	});

});



router.get('/files/search', function(req, res, next) {
	const files = [];

	var path = req.query.path ? pathUtil.normalize( req.query.path ) : '';
	var query = "*" + req.query.query + "*";

	console.log(pathUtil.join( root , path ), query);

	readdirp({ root: pathUtil.join( root , path ), fileFilter: query })
	  .on('data', function (entry) {
	  	files.push( getItemObject(pathUtil.join( path, entry.path )) );
	  }).on('end', function(){
			res.json({
				items: files
			});
	  })

});


router.get('/files', function(req, res, next) {
	const files = [];

	var path = req.query.path ? pathUtil.normalize( req.query.path ) : '';

	fs.readdirSync( pathUtil.join( root , path ) ).forEach(file => {
		files.push( getItemObject( pathUtil.join( path , file ) ) );
	});

	var current = getItemObject( path );
	var parent  = getItemObject( pathUtil.join(path,'..') );

	res.json({
		items: files,
		currentDir: current,
		parentDir: parent.fileName != '..' && current.fileName != '' ? parent : null,
	});

});


function getItemObject( path ) {
	var parsed = pathUtil.parse(path);
	var stat = fs.statSync( pathUtil.join( root , parsed.dir , parsed.base ) );

	return {
		id: parsed.base,
		fileName: parsed.base,
		path: pathUtil.join( parsed.dir , parsed.base ),
		dir: stat.isDirectory(),
		uploadedDate: stat.birthtimeMs,
		modifiedDate: stat.mtimeMs,
		url: 'http://localhost:4000/' + pathUtil.join( 'explorer', parsed.dir , parsed.base )
	};
}


module.exports = router;
