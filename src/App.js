import React, { Component } from "react";
import "./App.css";

import FileExplorer from "./components/FileExplorer";
import FileExplorerRepo from "./components/FileExplorerRepo";

class App extends Component {

  state = {
    selectedItems: []
  }

  render() {
    return (
      <div className="App">
        <FileExplorer 
          repo={new FileExplorerRepo()} 
          selectedItems={this.state.selectedItems} 
          onChange={(newSelectedItems) => { this.setState({selectedItems: newSelectedItems}); }}
        />
      </div>
    );
  }
}

export default App;
