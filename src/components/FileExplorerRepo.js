import axios from 'axios';


export default class FileExplorerRepo {

	base_host = 'http://localhost:4000';

	keys = {
		id: 'id',
		fileName: 'fileName',
		uploadedDate: 'uploadedDate',
		modifiedDate: 'modifiedDate',
		isDirectory: 'dir'
	}

	sortFields = {
		fileName: {
			type: 'string',
			label: 'File Name'
		},
		uploadedDate: {
			type: 'date',
			label: 'Uploaded Date'
		},
		modifiedDate: {
			type: 'date',
			label: 'Modified Date'
		}
	}

	/* Get initial directory, and files/folders */
	/* 
		Should return (resolve) a list of file/dir objects, of root folder
	*/
	getRoot() {
		return new Promise((resolve, reject) => {
			axios.get( this.base_host + '/files', {}).then((data) => {
				resolve({
					items: data.data.items,
					current: data.data.currentDir,
					parent: data.data.parentDir,
				});
			}).catch(() => {
				reject('Error');
			});
		});
	}

	/* 
		Param #1: Object of the dir from which the list of objects should be returned

		Should return (resolve) a list of file/dir objects in the given dir
		and parent dir
	*/
	getDir( dir ) {
		return new Promise((resolve, reject) => {
			axios.get( this.base_host + '/files', {params: {path: dir.path}}).then((data) => {
				resolve({
					items: data.data.items,
					current: data.data.currentDir,
					parent: data.data.parentDir,
				});
			}).catch(() => {
				reject('Error');
			});
		});
	}


	/*
		Param #1: Directory where files should be uploaded
		Param #2: File object for the file to be uploaded

		Return file object
	*/
	uploadFile( dir, file ) {
		return new Promise((resolve, reject) => {
			const formData = new FormData();
			console.log(file);
	    formData.append('file', file);
	    formData.append('path', dir.path);

	    axios.post( this.base_host + '/files/upload', formData).then(response => {
	      const data = response.data;
	      resolve(data.file);
	    }).catch(() => {
				reject('Error');
			});
	  });
	}

	/* 
		Param #1: Directory on which we should search
		Param #2: Find files matching this string

		Should return a list of files found
	*/
	searchDir( dir, query ) {
		return new Promise((resolve, reject) => {
			axios.get( this.base_host + '/files/search', {params: {path: dir.path, query: query}}).then((data) => {
				resolve({
					items: data.data.items
				});
			}).catch(() => {
				reject('Error');
			});
		});
	}

	/* 
		Param #1: Name of the of the dir to be created
		Param #2: Object of the dir where the other dir should be created

		Should return (resolve) the object of the created dir
	*/
	createDir( name, dir ) {
		return new Promise((resolve, reject) => {
			axios.post( this.base_host + '/files/create', {params: {dirName: name, path: dir.path}}).then((data) => {
				if( data.data.success ) {
					resolve(data.data.dir);
				} else {

					if( data.data.message ) {
						alert( data.data.message );
					}

					reject('Error');
				}
			}).catch(() => {
				reject('Error');
			});
		});
	}

	/*
		Param #1: Object of the dir, where files should be moved
		Param #2: Object of the files/dirs to be moved to given dir
	*/
	moveFiles( moveTo, files ) {
		return new Promise((resolve, reject) => {
			axios.get( this.base_host + '/files/move', {params: {to: moveTo.path, files: files}}).then((data) => {
				resolve({
					items: data.data.items
				});
			}).catch(() => {
				reject('Error');
			});
		});
	}

	/*
		Param #1: Object of the file/dir to be renamed
		Param #2: New name
	*/
	renameItem( file, newFilename ) {
		return new Promise((resolve, reject) => {
			axios.get( this.base_host + '/files/rename', {params: {file: file.path, newFilename: newFilename}}).then((data) => {
				resolve({
					item: data.data.item
				});
			}).catch(() => {
				reject('Error');
			});
		});
	}


	/*
		Param #1: Object of the file/dir to be deleted
	*/
	deleteItem( item ) {
		return new Promise((resolve, reject) => {
			axios.post( this.base_host + '/files/delete', {params: {path: item.path}}).then((data) => {

				if( data.data.success ) {
					resolve(data.data);
				} else {

					if( data.data.message ) {
						alert( data.data.message );
					}

					reject('Error');
				}

			}).catch(() => {
				reject('Error');
			});
		});
	}

}