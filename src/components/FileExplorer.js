import React, { Component } from "react";
import Dropzone from "react-dropzone";
import axios from "axios";
import moment from "moment";
import { 
  ContextMenu, 
  MenuItem as ContextMenuItem,
  ContextMenuTrigger 
} from "react-contextmenu";
import {
  Col,
  Row,
  Nav,
  Navbar,
  NavItem,
  NavDropdown,
  MenuItem,
  Panel,
  FormGroup,
  InputGroup,
  FormControl,
  Glyphicon
} from "react-bootstrap";

var containers = {
  main: 'main',
  tree: 'tree',
};

export default class FileExplorer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      current: null,
      files: [],
      searchResults: null,
      selectedFiles: [],
      selectedTreeFiles: [],
      draggingFrom: '',
      treeDirs: {},
      draggingItem: null,
      sort: null,
      previewItem: null,
      showSearch: false,
      showFilters: false,
      rootElement: {},
      selectedItems: []
    };
  }

  componentDidMount() {
    let { repo } = this.props;

    var sort = null;
    var sortKeys = Object.keys(repo.sortFields);
    if (sortKeys.length) {
      sort = sortKeys[0];
    }

    this.setState({
      sort: sort
    });

    repo.getRoot().then(data => {

      this.addTreeItems(data.current, data.items);

      this.setState({
        files: this.sortItems(data.items, this.state.sort),
        parent: data.parent,
        current: data.current,
        rootElement: data.current
      });
    });
  }

  render() {
    let { repo } = this.props;

    return (
      <div>
        <Navbar inverse collapseOnSelect className="fe-navbar">
          <Navbar.Header>
            <Navbar.Brand />
          </Navbar.Header>
          <Nav>
            <NavItem eventKey={1}>
              <div className="current-path">
                {!this.state.searchResults &&
                  this.state.current &&
                  this.state.current.path}
                {this.state.searchResults &&
                  this.state.current &&
                  `Showing search results for "${this.searchQuery}" in "${
                    this.state.current.path
                  }"`}
              </div>
            </NavItem>
          </Nav>
          <Nav>
            <NavItem
              eventKey={1}
              onClick={() =>
                this.setState({
                  showSearch: !this.state.showSearch
                })
              }
            >
              <Glyphicon glyph="search" />
            </NavItem>
            <NavDropdown
              id={'actions-dropdown'}
              eventKey={2}
              title={<Glyphicon glyph="option-vertical" />}
            >
              {!this.state.searchResults && (
                <MenuItem eventKey={2.1} onClick={e => this.createDir(e)}>
                  <Glyphicon glyph="plus" /> New folder
                </MenuItem>
              )}
              <MenuItem eventKey={2.2} onClick={e => this.dropzone.open(e)}>
                <Glyphicon glyph="cloud-upload" /> Upload files
              </MenuItem>
            </NavDropdown>
            <NavItem
              eventKey={3}
              onClick={() =>
                this.setState({
                  showFilters: !this.state.showFilters
                })
              }
            >
              <Glyphicon glyph="filter" />
            </NavItem>
          </Nav>
        </Navbar>

        <Panel
          expanded
          onToggle={() => {}}
          className={this.state.showSearch ? "fe-panel show" : "hide"}
        >
          <Panel.Collapse>
            <Panel.Body>
              <Col xs={12} md={6}>
                <FormGroup className="fe-form-group">
                  <InputGroup>
                    <FormControl
                      type="text"
                      value={this.state.value}
                      placeholder="Search"
                      onChange={this.search.bind(this)}
                    />
                    <InputGroup.Addon>
                      <Glyphicon glyph="search" />
                    </InputGroup.Addon>
                  </InputGroup>
                </FormGroup>
              </Col>
            </Panel.Body>
          </Panel.Collapse>
        </Panel>

        <Panel
          expanded
          onToggle={() => {}}
          className={this.state.showFilters ? "fe-panel show" : "hide"}
        >
          <Panel.Collapse>
            <Panel.Body>
              <Col xs={12} md={6}>
                <FormGroup
                  style={{ padding: 0 }}
                  controlId="fileformControlsSelect"
                  className="fe-form-group"
                >
                  <FormControl
                    onChange={e => {
                      this.setSort(e.target.value);
                    }}
                    componentClass="select"
                    placeholder="select"
                  >
                    <option value="">Sort by...</option>
                    {Object.keys(repo.sortFields).map(sort => {
                      var sortObject = repo.sortFields[sort];

                      return (
                        <option key={sort} value={sort}>
                          {sortObject.label}
                        </option>
                      );
                    })}
                  </FormControl>
                </FormGroup>
              </Col>
            </Panel.Body>
          </Panel.Collapse>
        </Panel>

        <Dropzone
          onDrop={this.handleDrop.bind(this)}
          multiple
          className={"dropzone"}
          acceptClassName={"dropzone-active"}
          disableClick={true}
          accept="image/*,text/*"
          ref={dropzone => {
            this.dropzone = dropzone;
          }}
        >
          <Row style={{ padding: 0 }}>
            <Col sm={4} md={3} className="fe-sidebar">
              {this.renderPreview()}
              {this.renderTree()}
              {this.renderSelectedItems()}
            </Col>

            <Col sm={8} md={9}>
              {this.renderItems()}
            </Col>
          </Row>
        </Dropzone>
      </div>
    );
  }

  renderSelectedItems() {
    let { repo, selectedItems=[] } = this.props;

    return (
      <div>
        {selectedItems.map((item,index) => {
          return (
            <div key={index}>
              {item[repo.keys.fileName]}
              <a href="#" onClick={() => {
                this.toggleSelected(item, false);
              }}>
                <i className={`glyphicon glyphicon-trash`} /> Remove
              </a>
            </div>
          );
        })}
      </div>
    )

  }

  renderTree() {
    let { repo } = this.props;

    return (
      <div className="tree">
        {this.renderTreeItems(this.state.rootElement[repo.keys.id], 0 )}
      </div>
    );
  }

  renderTreeItems( parentId, depth ) {
    let { repo } = this.props;

    var items = this.findTreeItemChildrens( parentId );

    if(!items || items.length === 0) return null;

    return (
      <ul className="tree-list">
        {items.map((item) => this.renderTreeItem(item, 1, depth))}
      </ul>
    );
  }

  renderTreeItem( item, itemIndex, depth ) {
    let { repo } = this.props;

    var props = {};

    var isCurrent = item[repo.keys.id] == this.state.current[repo.keys.id];

    if (itemIndex !== -1) {
      // props["onDragStart"] = e => {
      //   this.dragItem(e, item, itemIndex, containers.tree );
      // };
    }

    props["onDrop"] = e => {
      this.dropItem(e, item, itemIndex, containers.tree );
    };

    // props["onDragOver"] = e => {
    //   this.dragOverItem(e, item, itemIndex, containers.tree );
    // };

    props["onClick"] = e => {
      this.openItem(e, item, itemIndex, containers.tree );
    };

    var classes = ['tree-item', 'depth-'+depth];

    if( isCurrent ) {
      classes.push('current-tree-item');
    }

    return <li key={item[repo.keys.id]} className={"tree-list-item depth-" + depth}>
        <div className={classes.join(" ")}>
          <span onClick={() => this.toggleTreeItem(item)}>
            <span className="point">
              <i className={`glyphicon ${item.open ? "glyphicon-chevron-right" : "glyphicon-chevron-down"}`} />
            </span>
            <Glyphicon glyph={item.open ? "folder-open" : "folder-close"} />
          </span>
          <span style={{paddingLeft: 8}} {...props} draggable>
            {item[repo.keys.fileName]}
          </span>
        </div>
        {item.open && this.renderTreeItems(item[repo.keys.id], depth + 1)}
      </li>;
  }

  renderPreview() {
    if (!this.state.previewItem) return;

    return (
      <div className="preview">
        <span
          className="preview-close"
          onClick={() => {
            this.setState({
              previewItem: null
            });
          }}
        >
          Close Preview
        </span>
        <iframe title="file-preview" src={this.state.previewItem.url} allowtransparency="true" />
      </div>
    );
  }



  renderItems() {
    return (
      <div>
        {!this.state.searchResults &&
          this.state.parent &&
          this.renderItem(this.state.parent, -1)}

        {!this.state.searchResults &&
          this.state.files.map(this.renderItem.bind(this))}
        {this.state.searchResults &&
          this.state.searchResults.map(this.renderItem.bind(this))}
      </div>
    );
  }

  renderItem(item, itemIndex) {
    // itemIndex -1 is back item

    let { repo } = this.props;

    // var uploadedDate = moment(item[repo.keys.uploadedDate]).format();
    // var modifiedDate = moment(item[repo.keys.modifiedDate]).format();

    var props = {};

    if (itemIndex !== -1) {
      props["onDragStart"] = e => {
        this.dragItem(e, item, itemIndex, containers.main );
      };

      props["onClick"] = e => {
        this.selectItem(e, item, itemIndex, containers.main );
      };
    }

    props["onDrop"] = e => {
      this.dropItem(e, item, itemIndex, containers.main );
    };

    props["onDragOver"] = e => {
      this.dragOverItem(e, item, itemIndex, containers.main );
    };

    props["onDoubleClick"] = e => {
      this.openItem(e, item, itemIndex, containers.main );
    };

    var icon = '';

    if( item[repo.keys.isDirectory] ) {
      icon = 'folder-open';
    } else {
      icon = 'file';
    }

    return (
      <div
        draggable={itemIndex !== -1}
        className={
          this.state.selectedFiles.indexOf(item[repo.keys.id]) !== -1
            ? "selected"
            : ""
        }
        key={item[repo.keys.id]}
        {...props}
      >
        <Col sm={4} md={3} className="thumbnail text-center">
          <div className="item-icon">
            <ContextMenuTrigger id={""+itemIndex}>
              <i className={"glyphicon glyphicon-"+icon} />
            </ContextMenuTrigger>
          </div>

          {(itemIndex !== -1 && !item[repo.keys.isDirectory]) 
            && <input type="checkbox" checked={this.isItemSelected(item)} onChange={(e) => {
              this.toggleSelected(item, e.target.checked);
            }}/>
          }

          {itemIndex !== -1 && item[repo.keys.fileName]}
          {itemIndex === -1 && ".."}
          
          {itemIndex !== -1 && (
          <ContextMenu id={""+itemIndex}>
            <ContextMenuItem 
              onClick={e => {
                e.stopPropagation();
                this.renameItem(e, item, itemIndex);
              }}>
              <Glyphicon glyph="pencil" /> Rename
            </ContextMenuItem>
            <ContextMenuItem 
              onClick={e => {
                e.stopPropagation();
                this.deleteItem(e, item, itemIndex);
              }}>
              <Glyphicon glyph="trash" /> Delete
            </ContextMenuItem>
          </ContextMenu>)}

        </Col>
      </div>
    );
  }

  isItemSelected( item ) {
    let { repo, selectedItems=[] } = this.props;

    return selectedItems.filter((sitem) => sitem[repo.keys.id] == item[repo.keys.id]).length > 0;
  }

  toggleSelected( item, select ) {

    let { repo, selectedItems=[], onChange=() => {} } = this.props;

    var newSelectedItems;

    if( !select ) {
      newSelectedItems = selectedItems.filter((sitem) => (
        sitem[repo.keys.id] != item[repo.keys.id]
      ));
    } else {
      newSelectedItems = [...selectedItems, item];
    }

    onChange(newSelectedItems);
  }

  setTreeItemState( item, newItemState ) {
    let { repo } = this.props;

    let treeDirs = this.state.treeDirs;

    if( treeDirs[item[repo.keys.id]] ) {

      treeDirs[item[repo.keys.id]] = {
        ...treeDirs[item[repo.keys.id]],
        ...newItemState
      };

      this.setState({
        treeDirs: treeDirs
      })

    }
  }

  toggleTreeItem( item ) {
    if( item.open ) {
      this.setTreeItemState(item, {
        open: false
      })
    } else {
      this.openTreeItem( item );
    }
  }

  openTreeItem( item ) {
    let { repo } = this.props;

    this.setTreeItemState(item, {
      loading: true
    })

    repo.getDir(item).then(data => {

      this.addTreeItems(data.current, data.items);

      this.setTreeItemState(item, {
        loading: false,
        open: true
      })

    });
  }

  findTreeItemChildrens( parentId ) {
    return Object.keys(this.state.treeDirs)
      .filter((itemId) => this.state.treeDirs[itemId].parentId === parentId )
      .map((itemId) => this.state.treeDirs[itemId] );
  }

  deleteTreeItem( item ) {
    let { repo } = this.props;

    this.setState({
      treeDirs: Object.keys(this.state.treeDirs)
        .filter((itemId) => this.state.treeDirs[itemId][repo.keys.id] !== item[repo.keys.id] )
        .reduce((obj, itemId) => { 
          obj[itemId] = this.state.treeDirs[itemId];
          return obj; 
        }, {})
    });
  }

  renameTreeItems( item, itemName ) {
    let { repo } = this.props;

    this.setTreeItemState( item , {
      [repo.keys.fileName]: itemName
    })
  }

  addTreeItems( parent, items ) {
    let { repo } = this.props;

    var objItems = {};
    items.map((item) => {
      if( item[repo.keys.isDirectory] ) {
        objItems[item[repo.keys.id]] = {
          ...item,
          open: this.state.treeDirs[item[repo.keys.id]] ? this.state.treeDirs[item[repo.keys.id]].open : false,
          parentId: parent ?parent[repo.keys.id] :null,
        }
      }
      return item;
    })

    this.setState({
      treeDirs: {
        ...this.state.treeDirs,
        ...objItems
      }
    });

  }


  handleDrop(files) {
    let { repo } = this.props;

    const uploaders = files.map(file => {
      return repo
        .uploadFile(this.state.current, file)
        .then(fileObj => {
          this.setState({
            files: this.sortItems(
              [...this.state.files, fileObj],
              this.state.sort
            )
          });
        })
        .catch(() => {});
    });

    axios.all(uploaders).then(() => {
      // ... perform after upload is successful operation
    });
  }

  search(e) {
    this.searchQuery = e.target.value;

    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }

    this.searchTimeout = setTimeout(() => {
      if (this.searchQuery.trim() === "") {
        this.setState({
          selectedFiles: [],
          searchResults: null
        });
        return;
      }

      let { repo } = this.props;

      repo.searchDir(this.state.current, this.searchQuery).then(data => {
        this.setState({
          searchResults: this.sortItems(data.items, this.state.sort),
          selectedFiles: []
        });
      });
    }, 500);
  }

  setSort(sort) {
    this.setState({
      sort: sort,
      files: this.sortItems(this.state.files, sort)
    });

    if (this.state.searchResults) {
      this.setState({
        searchResults: this.sortItems(this.state.searchResults, sort)
      });
    }
  }

  sortItems(items, sort) {
    let { repo } = this.props;
    var sortObject = repo.sortFields[sort];

    return items.sort((a, b) => {
      if (sortObject.type === "string") {
        var textA = a[sort].toUpperCase();
        var textB = b[sort].toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      } else if (sortObject.type === "date") {
        var dateA = moment(a[sort]);
        var dateB = moment(b[sort]);
        return dateA.isBefore(dateB) ? -1 : dateA.isAfter(dateB) ? 1 : 0;
      }
      return 1;
    });
  }

  dragItem(e, item, itemIndex, container ) {
    let { repo } = this.props;

    var stateSelectedKey = container == containers.tree ? 'selectedTreeFiles' : 'selectedFiles';

    var sIndex = this.state[stateSelectedKey].indexOf(item[repo.keys.id]);

    if (sIndex === -1) {
      this.setState({
        [stateSelectedKey]: [item[repo.keys.id]]
      });
    }

    this.setState({
      draggingItem: item,
      draggingFrom: container
    });
  }

  dragOverItem(e, item, itemIndex) {
    if (!this.state.draggingItem) return;

    let { repo } = this.props;

    if (item[repo.keys.isDirectory] && this.state.draggingItem !== item) {
      e.preventDefault();
      e.stopPropagation();
    }
  }

  dropItem(e, item, itemIndex, container ) {
    let { repo } = this.props;

    var stateSelectedKey = this.state.draggingFrom == containers.tree ? 'selectedTreeFiles' : 'selectedFiles';
    var stateFilesKey = this.state.draggingFrom == containers.tree ? 'treeDirs' : 'files';

    if (
      !this.state.draggingItem ||
      this.state.draggingItem === item ||
      !item[repo.keys.isDirectory]
    )
      return;

    if( this.state.draggingFrom == containers.tree ) { 
      var moveFiles = [ this.state.treeDirs[ this.state.selectedTreeFiles ] ]
    } else {
      var moveFiles = this.state.files.filter((file, index) => {
        return this.state[stateSelectedKey].indexOf(file[repo.keys.id]) !== -1;
      });
    }

    repo.moveFiles(item, moveFiles).then(data => {

      if( item[repo.keys.id] == this.state.current[repo.keys.id] ) {
        this.setState({
          files: this.sortItems([...this.state.files, ...data.items], this.state.sort)
        })
      } else {
        this.setState({
          files: this.state.files.filter((file, itemIndex) => {
            return this.state[stateSelectedKey].indexOf(file[repo.keys.id]) === -1;
          })
        });
      }

      this.addTreeItems( item, data.items );
    });

    this.setState({
      draggingItem: null
    });
  }

  selectItem(e, item, itemIndex) {
    let { repo } = this.props;

    var ctrl = e.nativeEvent.ctrlKey;

    if (ctrl) {
      var sIndex = this.state.selectedFiles.indexOf(item[repo.keys.id]);
      if (sIndex === -1) {
        this.setState({
          selectedFiles: [...this.state.selectedFiles, item[repo.keys.id]]
        });
      } else {
        this.setState({
          selectedFiles: this.state.selectedFiles.filter(itemId => {
            return itemId !== item[repo.keys.id];
          })
        });
      }
    } else {
      this.setState({
        selectedFiles: [item[repo.keys.id]]
      });
    }
  }

  openItem(e, item, itemIndex) {
    let { repo } = this.props;

    if (item[repo.keys.isDirectory]) {
      repo.getDir(item).then(data => {

        this.addTreeItems(data.current, data.items);

        this.setTreeItemState(item, {
          open: true
        })

        this.setState({
          files: this.sortItems(data.items, this.state.sort),
          parent: data.parent,
          current: data.current,
          selectedFiles: []
        });
      });
    } else {
      this.setState({
        previewItem: item
      });
    }
  }

  renameItem(e, item, itemIndex) {
    let { repo } = this.props;

    var itemName = prompt("Enter item name");

    if (itemName) {
      repo.renameItem(item, itemName).then(data => {
        if( item ) {
          this.setState({
            files: this.state.files.map((mitem, mindex) => {
              if (item !== mitem) return mitem;
              return { ...mitem, ...data.item };
            })
          });

          this.deleteTreeItem(item);

          this.addTreeItems(this.state.current, [data.item]);
        }
      });
    }
  }

  deleteItem(e, item, itemIndex) {
    let { repo } = this.props;

    repo.deleteItem(item).then(data => {
      this.setState({
        files: this.state.files.filter((file, dIndex) => {
          return item[repo.keys.id] !== file[repo.keys.id];
        })
      });

      this.deleteTreeItem( item );
    });
  }

  createDir(e) {
    let { repo } = this.props;

    var dirName = prompt("Enter directory name");

    if (dirName) {
      repo
        .createDir(dirName, this.state.current)
        .then(dir => {
          this.setState({
            files: this.sortItems([...this.state.files, dir], this.state.sort)
          });

          this.addTreeItems(this.state.current, [dir]);
        })
        .catch(() => {});
    }
  }
}
